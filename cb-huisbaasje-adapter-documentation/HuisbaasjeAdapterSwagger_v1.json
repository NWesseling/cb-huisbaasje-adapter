swagger: '2.0'
info:
  description: >-
    De HuisbaasjeAdapter levert op het moment 2 functionaliteiten:
    leverRelatie en wijzigRelatie.
  version: 1.0.0-SNAPSHOT
  title: HuisbaasjeAdapterService
  termsOfService: 'http://www.consumentenbond.nl'
  contact:
    email: nwesseling@consumentenbond.nl
host: test-services.test-cb.nl
basePath: /adapters/huisbaasje/v1
tags:
  - name: relatie
    description: ophalen en wijzigen van een Relatie.
schemes:
  - https
paths:
  '/{relatienummer}':
    get:
      tags:
        - relaties
      summary: Opvragen relatie aan de hand van een relatienummer
      description: Retourneert een enkele Relatie
      operationId: leverRelatie
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: relatienummer
          in: path
          description: Relatienummer wat de relatie uniek identificeert
          required: true
          type: string
          minLength: 10
          maxLength: 10
          pattern: '^\d{10}$'
      responses:
        '200':
          description: Relatie succesvol opgehaald
          schema:
            $ref: '#/definitions/leverRelatieResponse'
  '/wijzigRelatie':
    post:
      tags:
        - relatie
      summary: Vult velden van relatie
      description: Vult voorletters, tussenvoegsels, achternaam en geslacht. Alleen velden die nog niet bekend zijn bij relatie mogen worden meegegeven.
      operationId: wijzigRelatie
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: >-
            De velden die toegevoegd kunnen worden aan een relatie. 
            Alleen velden die nog niet vasgelegd zijn voor de relatie mogen hierbij meekomen.
          required: true
          schema:
            $ref: '#/definitions/WijzigRelatieRequest'
      responses:
        '200':
          description: Resultaat wijziging van velden
          schema:
            $ref: '#/definitions/wijzigRelatieResponse'
        '500':
          description: Functionele fout of technische fout
          schema:
            $ref: '#/definitions/foutmeldingResponseType'
            
  '/activeerHuisbaasje':
    post:
      tags:
        - relatienummer
      summary: activeert huisbaasje voor de relatie
      description: activeert huisbaasje voor de relatie indien nog niet geactiveerd. Indien huisbaasje al actief is wordt geen actie op basis van huisbaasje aanroep ondernomen.
      operationId: activeerHuisbaasje
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: >-
            De relatienummer van de relatie waarvoor huisbaasje actief dient te worden.
          required: true
          schema:
            $ref: '#/definitions/activeerHuisbaasjeRequest'
      responses:
        '200':
          description: Resultaat van activatie
          schema:
            $ref: '#/definitions/activeerHuisbaasjeResponse'
        '500':
          description: Functionele fout of technische fout
          schema:
            $ref: '#/definitions/foutmeldingResponseType'

definitions:
  activeerHuisbaasjeRequest:
    type: object
    required:
      - relatienummer
    properties:
      relatienummer:
        $ref: '#/definitions/relatienummerType'

  activeerHuisbaasjeResponse:
    type: object
    required:
      - geactiveerd
    properties:
      geactiveerd:
        $ref: '#/definitions/jaNeeDom'
        
  leverRelatieRequest:
    type: object
    required:
      - relatienummer
    properties:
      relatienummer:
        $ref: '#/definitions/relatienummerType'
        
  leverRelatieResponse:
    type: object
    properties:
      relatie:
        $ref: '#/definitions/Relatie'

  WijzigRelatieRequest:
    type: object
    required:
      - relatienummer
    properties:
      relatienummer:
        $ref: '#/definitions/relatienummerType'
      voorletters:
        $ref: '#/definitions/voorlettersType'
      tussenvoegsels:
        $ref: '#/definitions/tussenvoegselsType'
      achternaam:
        $ref: '#/definitions/achternaamType'
      geslacht:
        $ref: '#/definitions/geslachtDom'
    xml:
      name: WijzigRelatieRequest
  
  wijzigRelatieResponse:
    type: object
    properties:
      relatienummer:
        $ref: '#/definitions/relatienummerType'
      
  Relatie:
    type: object
    required:
      - emailadres
    properties:
      relatienummer:
        $ref: '#/definitions/relatienummerType'
      voorletters:
        $ref: '#/definitions/voorlettersType'
      tussenvoegsels:
        $ref: '#/definitions/tussenvoegselsType'
      achternaam:
        $ref: '#/definitions/achternaamType'
      geslacht:
        $ref: '#/definitions/geslachtDom'
    xml:
      name: Relatie
  
  foutmeldingResponseType:
    type: object
    required:
      - code
      - omschrijving
      - specificatie
      
    properties:
      code:
        type: string
        pattern: '^\d{3}$'
        description: Identificeert foutmelding
      omschrijving:
        type: string
      specificatie:
        type: string
        
  relatienummerType:
    type: string
    minLength: 10
    maxLength: 10
    pattern: '^\d{10}$'
    description: Identificeert de relatie
  
  voorlettersType:
    type: string
    maxLength: 20
    pattern: ^(\D\.)*$
    description: must be all caps with dots in between
  
  tussenvoegselsType:
    type: string
    maxLength: 20
  
  achternaamType:
    type: string
    maxLength: 40
    pattern: '^[A-Z]\D+$'
    description: 'must be first-letter capital, rest lowercase'
  
  geslachtDom:
    type: string
    enum:
      - ONBEKEND
      - MAN
      - VROUW
  
  jaNeeDom:
    type: string
    enum:
      - JA
      - NEE
  
externalDocs:
  description: Find out more about Swagger
  url: 'http://swagger.io'
