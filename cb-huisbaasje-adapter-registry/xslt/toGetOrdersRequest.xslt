<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:v1="http://nl.consumentenbond/service/orderservice/v1">
	<xsl:template match="/">
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://nl.consumentenbond/service/orderservice/v1">
   		<soapenv:Header/>
   		<soapenv:Body>
	      <v1:getOrdersRequest>
	      	 <v1:RelationId>
	         	<xsl:value-of select="//relatienummer"/>
	         </v1:RelationId>
      	  </v1:getOrdersRequest>
   		</soapenv:Body>
	  </soapenv:Envelope>
	</xsl:template>
</xsl:stylesheet>