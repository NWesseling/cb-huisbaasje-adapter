<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
    <!-- neem alleen velden mee die vanuit huisbaasje gewijzigd mogen worden.. -->
	<xsl:template match="relatie">
	   <relatie>
	       <herkomst><xsl:text>L01</xsl:text></herkomst>

	       <xsl:copy-of select="relatienummer"/>
 	       <xsl:copy-of select="voorletters"/>
           <xsl:copy-of select="tussenvoegsels"/>
           <xsl:copy-of select="achternaam"/>
           
           <xsl:copy-of select="geslacht"/>
          
	   </relatie>
	</xsl:template>
			
</xsl:stylesheet>