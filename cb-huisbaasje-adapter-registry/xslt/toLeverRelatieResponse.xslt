<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<xsl:apply-templates select="jsonObject"/>
	</xsl:template>
	
	<xsl:template match="jsonObject">
		<xsl:apply-templates select="//relatie"/>
	</xsl:template>
	
    <!-- Geef alleen velden mee die vanuit huisbaasje getoond dienen te worden.. -->
	<xsl:template match="relatie">
	   <relatie>
	   	  <xsl:copy-of select="voorletters"/>
           <xsl:copy-of select="tussenvoegsels"/>
           <xsl:copy-of select="achternaam"/>
           
           <xsl:copy-of select="geslacht"/>
	   </relatie>
	</xsl:template>
</xsl:stylesheet>
