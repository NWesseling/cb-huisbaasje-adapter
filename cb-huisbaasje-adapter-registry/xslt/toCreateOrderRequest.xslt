<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- xsl:param name="promotionchannel"/-->
	
	<xsl:template match="/">
		<xsl:apply-templates select="jsonObject"/>
	</xsl:template>
	
	<xsl:template match="jsonObject">
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://nl.consumentenbond/service/orderservice/v1" xmlns:v11="http://nl.consumentenbond/data/order/v1" xmlns:v12="http://nl.consumentenbond/data/product/v1">
		   <soapenv:Header/>
		   <soapenv:Body>
		      <v1:createOrderRequest>
		         <v1:Order>
		            <v11:RelationId>
		            	<xsl:value-of select="relatienummer"/>
		            </v11:RelationId>
		            <!--Optional:-->
		            <v11:PayerId>
		            	<xsl:value-of select="relatienummer"/>
		            </v11:PayerId>
		            <!--Optional:-->
		            <v11:ReceiverId>
		            	<xsl:value-of select="relatienummer"/>
		            </v11:ReceiverId>
		            <v11:PaymentType>X</v11:PaymentType>
		            <v11:OrderDate>
		            	<xsl:value-of select="format-date(current-date(), '[Y0001]-[M01]-[D01]')"/>
		            </v11:OrderDate>
		            
		            
		            <v11:PromotionChannel>
		            	<xsl:value-of select="initiatief"/>	
		            </v11:PromotionChannel>
		            
		            
		             
		            <v11:MediumCode>ELEC</v11:MediumCode>
		            <!--Optional:-->
		            <v11:UserName>WSO2</v11:UserName>
		            <!--1 or more repetitions:-->
		            <v11:OrderLine>
		               <v11:Product>
		                  <v12:ProductCode>
		                  	<xsl:text>AR002</xsl:text>
		                  </v12:ProductCode>
		               </v11:Product>
		               <v11:Quantity>1</v11:Quantity>
		               <!--Optional:-->
		               <v11:WBS>
		               		<xsl:text>C.0032.0004</xsl:text>
		               </v11:WBS>
		            </v11:OrderLine>
		         </v1:Order>
		      </v1:createOrderRequest>
		   </soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>
</xsl:stylesheet>